$(document).ready(function(){
	var xAttr = '';
	var x = 'x';
	var y = 'y';

	//Data for main block
	var mainImgPath1 = '#slider_block #main1 img';
	var mainImgPath2 = '#slider_block #main2 img';
	var mainBlockPath1 = '#slider_block #main1';
	var mainBlockPath2 = '#slider_block #main2';

	//Data for minor1 block
	var minorImgPath1 = '#slider_block #minor1 img';
	var minorImgPath2 = '#slider_block #minor2 img';
	var minorBlockPath1 = '#slider_block #minor1';
	var minorBlockPath2 = '#slider_block #minor2';

	//Data for minor2 block
	var minorImgPath3 = '#slider_block #minor3 img';
	var minorImgPath4 = '#slider_block #minor4 img';
	var minorBlockPath3 = '#slider_block #minor3';
	var minorBlockPath4 = '#slider_block #minor4';

	var fx = 1;
	flagx = true;

	var fy1 = 2;
	flagy1 = true;

	var fy2 = 3;
	flagy2 = true;

	setInterval(function(){
		xarr = slider(fx, x, mainImgPath1, mainImgPath2, mainBlockPath1, mainBlockPath2, flagx);
		fx = xarr[0];
		flagx = xarr[1];

		y1arr = slider(fy1, y, minorImgPath1, minorImgPath2, minorBlockPath1, minorBlockPath2, flagy1);
		fy1 = y1arr[0];
		flagy1 = y1arr[1];

		y2arr = slider(fy2, y, minorImgPath3, minorImgPath4, minorBlockPath3, minorBlockPath4, flagy2);
		fy2 = y2arr[0];
		flagy2 = y2arr[1];
	}, 8000);

	function slider(f, x_or_y, imgPath1, imgPath2, blockPath1, blockPath2, flag) {
		if(f < 3) {
			if(flag) {
				xAttr = 'images/slider/' + ++f + '_' + x_or_y + '.jpg';

				$(imgPath2).attr('src', xAttr);

				$(blockPath1).fadeOut(400);
				$(blockPath2).fadeIn(400);

				flag = false;
			}
			else {
				xAttr = 'images/slider/' + ++f + '_' + x_or_y + '.jpg';

				$(imgPath1).attr('src', xAttr);

				$(blockPath2).fadeOut(400);
				$(blockPath1).fadeIn(400);

				flag = true;
			}
		}
		else {
			f = 1;
			if(flag) {
				xAttr = 'images/slider/' + f + '_' + x_or_y + '.jpg';

				$(imgPath2).attr('src', xAttr);

				$(blockPath1).fadeOut(400);
				$(blockPath2).fadeIn(400);

				flag = false;
			}
			else {
				xAttr = 'images/slider/' + f + '_' + x_or_y + '.jpg';

				$(imgPath1).attr('src', xAttr);

				$(blockPath2).fadeOut(400);
				$(blockPath1).fadeIn(400);

				flag = true;
			}
		}
		var arr = [f, flag];
		return arr;
	}

});